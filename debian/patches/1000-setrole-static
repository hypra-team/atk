commit f1a50c016dba0b23f2b5ccf2dec642f63fafc221
Author: Samuel Thibault <sthibault@hypra.fr>
Date:   Tue Mar 20 16:22:38 2018 +0100

    atkobject: fix set_property for accessible-role
    
    accessible-role was wrongly defines as being of type INT instead of
    type ENUM.

commit 30399c42a524af14a48a227ccd5f588ac54b51eb
Author: Alejandro Piñeiro <apinheiro@igalia.com>
Date:   Thu Mar 22 15:24:00 2018 +0100

    atkobject: fix get_property for accessible-role
    
    Previous commit fixed set_property for accessible-role (as it is a
    enum instead of an int). As Rico Tzschichholz pointed on bug 794513,
    get_property was not updated accordingly.

diff --git a/atk/atkobject.c b/atk/atkobject.c
index fb61bab..282c604 100644
--- a/atk/atkobject.c
+++ b/atk/atkobject.c
@@ -408,11 +408,10 @@ atk_object_class_init (AtkObjectClass *klass)
                                                         G_PARAM_READWRITE));
   g_object_class_install_property (gobject_class,
                                    PROP_ROLE,
-                                   g_param_spec_int    (atk_object_name_property_role,
+                                   g_param_spec_enum   (atk_object_name_property_role,
                                                         _("Accessible Role"),
                                                         _("The accessible role of this object"),
-                                                        0,
-                                                        G_MAXINT,
+                                                        ATK_TYPE_ROLE,
                                                         ATK_ROLE_UNKNOWN,
                                                         G_PARAM_READWRITE));
   g_object_class_install_property (gobject_class,
@@ -1310,7 +1309,7 @@ atk_object_real_set_property (GObject      *object,
       atk_object_set_description (accessible, g_value_get_string (value));
       break;
     case PROP_ROLE:
-      atk_object_set_role (accessible, g_value_get_int (value));
+      atk_object_set_role (accessible, g_value_get_enum (value));
       break;
     case PROP_PARENT:
       atk_object_set_parent (accessible, g_value_get_object (value));
@@ -1350,7 +1350,7 @@ atk_object_real_get_property (GObject      *object,
       g_value_set_string (value, atk_object_get_description (accessible));
       break;
     case PROP_ROLE:
-      g_value_set_int (value, atk_object_get_role (accessible));
+      g_value_set_enum (value, atk_object_get_role (accessible));
       break;
     case PROP_LAYER:
       if (ATK_IS_COMPONENT (accessible))
